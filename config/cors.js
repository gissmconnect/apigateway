module.exports = {
    origin: "*",
    methods: ["GET", "OPTIONS", "POST", "PUT", "DELETE"],
    allowedHeaders: ["content-type", "access-control-allow-origin","Authorization"],
    exposedHeaders: ["access-control-allow-origin"],
    credentials: false,
    maxAge: 3600
};
