module.exports = {
  apps: [
    {
      name: 'apigateway',
      script: 'moleculer-runner --repl --hot services/**/*.service.js',
      exec_mode: 'fork',
      // instances: 0,
      kill_timeout: 3000,
      wait_ready: true,
      listen_timeout: 3000,
      exp_backoff_restart_delay: 100,
      env: {
        NODE_ENV: 'dev',
        TRANSPORTER: 'Kafka'
      },
      time: true
    }
  ]
};
