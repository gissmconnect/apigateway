module.exports = {
  apps: [
    {
      name: 'apigateway',
      script: 'moleculer-runner --repl --hot --env services/**/*.service.js',
      exec_mode: 'fork',
      watch: true,
      env: {
        NODE_ENV: 'default',
        TRANSPORTER: 'Kafka'
      },
      time: true
    }
  ]
};
