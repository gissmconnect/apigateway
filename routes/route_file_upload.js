var multiparty = require('multiparty');
// const request = require('request')
var fs = require('fs');

module.exports = {
    path: "/v1/file",
    authorization: true,
    aliases: {
        "POST upload":"ocr.read_ocr",
        "POST upload/marksheet":"ocr.read_marksheet",
        "POST upload/sample":"ocr.sample_ocr"
    },
    mappingPolicy: "restrict",
    // Use bodyparser module
    mergeParams: false,
    bodyParsers: require('../config/default_bodyParsers.js'),
    onBeforeCall(ctx,route,req, res){
            const form = new multiparty.Form();
            return new Promise(function(resolve, reject){
           return form.parse(req, function (err, fields, files) {
                const file = files.file[0];
                ctx.meta.file = file;
                //file will be stored in temp folder, read and write from there
                console.log(file)
                resolve();
           })
            
        })
    },
    onAfterCall(ctx, route, req, res, data) {
        // Async function which return with Promise
        if(data.success==false){
          console.log("in false condition")
          res.writeHead(data.statusCode);
          return res.end(JSON.stringify(data))
        }
        return data;
      },
      onError(req, res, err) {
        res.setHeader("Content-Type", "application/json");
        res.writeHead(err.code || 500);
        let resObj = {
          status: false,
          statusCode: err.code || 500,
          message: err.message,
          payload: err.data || {}
        }
        res.end(JSON.stringify(resObj));
      }
    };




