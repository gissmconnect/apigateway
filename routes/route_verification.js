const Middleware = require("../handler/middleware");
module.exports = {
  path: "/user",
  authorization: true,
  //authorization: true,
   //  roles: ["ADMIN1"]
  aliases: {
    "POST verification" : "verification.sendOtp",   
    "POST verification/otp":"verification.verifyOtp" 
  },
  mappingPolicy: "restrict",
  bodyParsers: require('../config/default_bodyParsers.js'),
  
  onBeforeCall(ctx, route, req, res) {
    this.logger.info("onBeforeCall in protected route");
  },
  onAfterCall(ctx, route, req, res, data) {
    // Async function which return with Promise
    if(data.success==false){
      console.log("in false condition")
      res.writeHead(data.statusCode);
      return res.end(JSON.stringify(data))
    }
    return data;
  },
  onError(req, res, err) {
    res.setHeader("Content-Type", "application/json");
    res.writeHead(err.code || 500);
    let resObj = {
      status: false,
      statusCode: err.code || 500,
      message: err.message,
      payload: err.data || {}
    }
    res.end(JSON.stringify(resObj));
  }
};
