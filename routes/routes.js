const Middleware = require("../handler/middleware");
module.exports = {
  path: "/v1",
  authorization: true,
  //authorization: true,
   //  roles: ["ADMIN1"]
  aliases: {
    "POST login" : "auth.student_login",
    "GET student" : "students.get_students",
    "GET student/check" : "students.check_student",
    "POST student/registration" : "students.insert_student",
    "POST student/address/insert":"students.insert_address",
    "POST student/category/insert":"students.insert_category",
    "POST student/get":"students.get_all_detail",
    "POST student/address":"students.get_address",
    "POST student/category":"students.get_category",
    "POST student/update":"students.update_student",
    "GET student/get":"students.get_student",
    "POST subject/add":"subject.add_subject",
    "GET subject/get":"subject.get_subject",
    "GET marks/get":"marks.get_student_marks",
    "POST marks/update":"marks.update_student_marks",
    "POST marks/add":"marks.add_student_marks",
    "POST login/add":"students.store_login_detail"

    
  },
  mappingPolicy: "restrict",
  bodyParsers: require('../config/default_bodyParsers.js'),
  
  onBeforeCall(ctx, route, req, res) {
    this.logger.info("onBeforeCall in protected route");
  },
  onAfterCall(ctx, route, req, res, data) {
    // Async function which return with Promise
    if(data.success==false){
      console.log("in false condition")
      res.writeHead(data.statusCode);
      return res.end(JSON.stringify(data))
    }
    return data;
  },
  onError(req, res, err) {
    res.setHeader("Content-Type", "application/json");
    res.writeHead(err.code || 500);
    let resObj = {
      status: false,
      statusCode: err.code || 500,
      message: err.message,
      payload: err.data || {}
    }
    res.end(JSON.stringify(resObj));
  }
};
