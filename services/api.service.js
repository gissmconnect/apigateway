"use strict";

const fs = require('fs');
const ApiGateway = require("moleculer-web");
const compression = require("compression");
const cookieParser = require("cookie-parser");
// const privateKEY = process.env.JWT_SECRET_KEY || "";
const dotenv = require("dotenv-safe");
const { UnAuthorizedError } = ApiGateway.Errors;
dotenv.config();

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 * @typedef {import('http').IncomingMessage} IncomingRequest Incoming HTTP Request
 * @typedef {import('http').ServerResponse} ServerResponse HTTP Server Response
 */

// Load Routes
let files = fs.readdirSync(__dirname + '/../routes');
let _routes = [];

for (let i = 0; i < files.length; i++) {
	let file = files[i];
	_routes = [..._routes, require(`../routes/${file}`)];
}

module.exports = {
	name: "api",
	mixins: [ApiGateway],
	middleware: false,
	use: [compression(), cookieParser()],
	logRequestParams: "info",
	logResponseData: "info",

	settings: {
		port: process.env.PORT || 3000,
		cors: require('../config/cors.js'),

		routes: [
			..._routes
		]
	},
	methods: {
		/**
		 * Authorize the request
		 *
		 * @param {Context} ctx
		 * @param {Object} route
		 * @param {IncomingRequest} req
		 * @returns {Promise}
		 */
		async authorize(ctx, route, req) {
			let token;
			if (req.headers.authorization) {
				let type = req.headers.authorization.split(" ")[0];
				if (type === "Token" || type === "Bearer")
					token = req.headers.authorization.split(" ")[1];
			}

			let user;
			if (token) {
				// Verify JWT token
				try {
					user = await ctx.call("students.resolveToken", { token });
					console.log(user,"KKKKKKK")
					if (user) {
						this.logger.info("Authenticated via JWT: ", user.payload.CIVIL_NUM);
						// Reduce user fields (it will be transferred to other nodes)
						ctx.meta.user = user.payload.CIVIL_NUM
						ctx.meta.token = token;
					}
				} catch (err) {
					// Ignored because we continue processing if user doesn't exists
				}
			}

			if (req.$action.auth == "required" && !user)
				throw new UnAuthorizedError();
		}
	}
};
